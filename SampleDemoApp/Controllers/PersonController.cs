﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;
using SampleDemoApp.Models;
using SampleDemoApp.ViewModel;
using AutoMapper;

namespace SampleDemoApp.Controllers
{
    /// <summary>
    /// Person Controller 
    /// Main controller of the application
    /// Contains all the action required for handling Person
    /// </summary>
    [HandleError(View = "Error")]
    public class PersonController : Controller
    {
        private MyDBContext _context;

        public PersonController()
        {
            //Intialize DB Context object
            _context = new MyDBContext();
        }


        protected override void Dispose(bool disposing)
        {
            //Dispose DB context object
            _context.Dispose();
        }

        // GET: Person List Page
        [HttpGet]
        public ActionResult Index()
        {
            var persons = _context.Persons.Include(c => c.UserStatus).Include(c=>c.UserType).ToList();
            return View(persons);
        }

        // GET: Person Details Page
        [HttpGet]
        public ActionResult View(int id)
        {
            try
            {
                var user = _context.Persons.Include(c => c.UserStatus).Include(c => c.UserType).Single(m => m.Id == id);
                return View("Details", user);
            }
            catch
            {
                return View("Error", new ErrorViewModel()
                {
                    ErrorName = "Person Not Found",
                    ErrorMessage = "The Person you are searching is not found"
                });
            }
        }

        // GET: Add New Person Page
        [HttpGet]
        public ActionResult AddUser()
        {
            ViewBag.PageName = "Add New Person";
            var viewModel = new PersonFormViewModel();
            viewModel.Person = new Person();
            viewModel.UserTypes = _context.UserTypes.ToList();
            viewModel.UserStatus = _context.UserStatus.ToList();
            return View("PersonForm", viewModel);
        }

        // Post: Save method for adding and editing Person
        [HttpPost]
        public ActionResult Save(PersonFormViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    viewModel.UserTypes = _context.UserTypes.ToList();
                    viewModel.UserStatus = _context.UserStatus.ToList();
                    return View("PersonForm", viewModel);
                }
                if (viewModel.Person.Id == 0)
                {
                    ViewBag.PageName = "Add New Person";
                    _context.Persons.Add(viewModel.Person);
                }
                else
                {
                    var personInDb = _context.Persons.Single(m => m.Id == viewModel.Person.Id);
                    Mapper.Map(viewModel.Person, personInDb);
                }
                _context.SaveChanges();
                return RedirectToAction("Index", "Person");
            }
            catch
            {
                return View("Error", new ErrorViewModel()
                {
                    ErrorName = "Oops",
                    ErrorMessage = "Something went wrong please try contact Admin"
                });
            }

        }

        // GET: Add New Person Page
        [HttpGet]
        public ActionResult Edit(int id)
        {
            try
            {
                var person = _context.Persons.Single(p => p.Id == id);
                ViewBag.PageName = "Edit Person";
                var viewModel = new PersonFormViewModel();
                viewModel.UserTypes = _context.UserTypes.ToList();
                viewModel.UserStatus = _context.UserStatus.ToList();
                viewModel.Person = person;
                return View("PersonForm", viewModel);
            }
            catch
            {
                return View("Error", new ErrorViewModel()
                {
                    ErrorName = "Person Not Found",
                    ErrorMessage = "Person you are searching is not found"
                });
            }

        }

        // Post: Delete method for deleting Person record
        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                var user = _context.Persons.Single(m => m.Id == id);
                _context.Persons.Remove(user);
                _context.SaveChanges();
                return Json(new { status = "Success" }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { status = "Failed" }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}