﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SampleDemoApp.ViewModel;

namespace SampleDemoApp.Controllers
{
    public class ErrorController : Controller
    {
        // GET: Error
        public ActionResult Index()
        {
            return View("Error",new ErrorViewModel() {
                ErrorName="Oops",
                ErrorMessage= "Something went wrong please contact Admin"
            });
        }
    }
}