﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SampleDemoApp.Models;
using AutoMapper;

namespace SampleDemoApp
{
    public static class AutoMapperConfig
    {
        public static void ConfigureMaps()
        {
            AutoMapper.Mapper.Initialize(cfg => {
                cfg.CreateMap<Person, Person>();
                /* etc */
            });
        }
        
    }
}