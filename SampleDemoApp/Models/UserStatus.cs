﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SampleDemoApp.Models
{
    public class UserStatus
    {
        public int id { get; set; }
        public string UserStatusName { get; set; }
    }
}