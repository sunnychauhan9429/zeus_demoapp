﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SampleDemoApp.Models
{
    public class Person
    {
        public int Id { get; set; }

        [Required]
        [Display(Name ="First Name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Email Address")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string EmailId { get; set; }

        [Required]        
        public string Password { get; set; }

        [Display(Name ="Date of Birth")]
        public DateTime? DOB { get; set; }

        [Required]        
        public string City { get; set; }

        
        public UserType UserType { get; set; }
        
        public UserStatus UserStatus { get; set; }

        [Required]
        [Display(Name = "User Type")]
        public int UserTypeId { get; set; }

        [Required]
        [Display(Name = "User Status")]
        public int UserStatusId { get; set; }

    }
}