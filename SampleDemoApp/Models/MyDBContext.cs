﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace SampleDemoApp.Models
{
    public class MyDBContext :DbContext
    {
        public DbSet<Person> Persons { get; set; }
        public DbSet<UserType> UserTypes { get; set; }
        public DbSet<UserStatus> UserStatus { get; set; }

        public MyDBContext() : base("DefaultConnection")
        {

        }
        public static MyDBContext Create()
        {
            return new MyDBContext();
        }
    }
}