﻿$(document).ready(function () {
    $("#loading-img").addClass("hidden");

    $(document).on("click", "#btn-back", function () {
        $("#loading-img").removeClass("hidden");
        $(location).attr("href", location.protocol + "//" + location.host + "/person/index");
    });

    $(document).on("click", ".delete-person", function (e) {
        var id = $(e.currentTarget).attr("id");
        var row = $(this);
        bootbox.confirm("Are you sure you want to delete this Person?", function (result) {
            if (result) {                
                $.ajax({
                    url: "/person/delete/" + id,
                    method: "POST",
                    success: function (result) {
                        if (result.status === "Success")
                        {
                            row.parents("tr").remove();//delete row of the deleted record
                            bootbox.alert("Person deleted successfully", function () {
                                if ($("tbody tr").length === 0)
                                {
                                    //if no more records redirect to home page again
                                    $(location).attr("href", location.protocol + "//" + location.host + "/person/index");
                                }
                            });
                        }
                        else
                        {
                            bootbox.alert("Unable to delete person", null);
                        }

                        
                    }, error: function (errormsg) {
                        bootbox.alert("Something went wrong");
                    }
                });
            }
        });
    });

    $(document).on("click", ".view-person", function () {
        $("#loading-img").removeClass("hidden");
    });

})