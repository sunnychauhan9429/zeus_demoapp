﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SampleDemoApp.ViewModel
{
    public class ErrorViewModel
    {
        public string ErrorName { get; set; }
        public string ErrorMessage { get; set; }
    }
}