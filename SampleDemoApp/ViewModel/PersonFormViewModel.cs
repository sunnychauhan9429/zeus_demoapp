﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SampleDemoApp.Models;

namespace SampleDemoApp.ViewModel
{
    public class PersonFormViewModel
    {
        public IEnumerable<UserType> UserTypes { get; set; }
        public IEnumerable<UserStatus> UserStatus { get; set; }
        public Person Person { get; set; }
    }
}