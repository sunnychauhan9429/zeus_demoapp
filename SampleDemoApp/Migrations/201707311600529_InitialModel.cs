namespace SampleDemoApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PersonModels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        EmailId = c.String(nullable: false),
                        Password = c.String(nullable: false),
                        DOB = c.DateTime(nullable: false),
                        City = c.String(nullable: false),
                        UserStatus_id = c.Int(nullable: false),
                        UserType_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserStatus", t => t.UserStatus_id, cascadeDelete: true)
                .ForeignKey("dbo.UserTypes", t => t.UserType_id, cascadeDelete: true)
                .Index(t => t.UserStatus_id)
                .Index(t => t.UserType_id);
            
            CreateTable(
                "dbo.UserStatus",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        UserStatusName = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.UserTypes",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        UserTypeName = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PersonModels", "UserType_id", "dbo.UserTypes");
            DropForeignKey("dbo.PersonModels", "UserStatus_id", "dbo.UserStatus");
            DropIndex("dbo.PersonModels", new[] { "UserType_id" });
            DropIndex("dbo.PersonModels", new[] { "UserStatus_id" });
            DropTable("dbo.UserTypes");
            DropTable("dbo.UserStatus");
            DropTable("dbo.PersonModels");
        }
    }
}
