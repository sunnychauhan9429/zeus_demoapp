namespace SampleDemoApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class makeDOBnullable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.People", "DOB", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.People", "DOB", c => c.DateTime(nullable: false));
        }
    }
}
