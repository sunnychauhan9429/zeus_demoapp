namespace SampleDemoApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changePersonTableName : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.PersonModels", newName: "People");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.People", newName: "PersonModels");
        }
    }
}
