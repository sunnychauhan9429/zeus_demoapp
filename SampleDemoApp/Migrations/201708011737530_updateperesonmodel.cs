namespace SampleDemoApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateperesonmodel : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.People", name: "UserStatus_id", newName: "UserStatusId");
            RenameColumn(table: "dbo.People", name: "UserType_id", newName: "UserTypeId");
            RenameIndex(table: "dbo.People", name: "IX_UserType_id", newName: "IX_UserTypeId");
            RenameIndex(table: "dbo.People", name: "IX_UserStatus_id", newName: "IX_UserStatusId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.People", name: "IX_UserStatusId", newName: "IX_UserStatus_id");
            RenameIndex(table: "dbo.People", name: "IX_UserTypeId", newName: "IX_UserType_id");
            RenameColumn(table: "dbo.People", name: "UserTypeId", newName: "UserType_id");
            RenameColumn(table: "dbo.People", name: "UserStatusId", newName: "UserStatus_id");
        }
    }
}
